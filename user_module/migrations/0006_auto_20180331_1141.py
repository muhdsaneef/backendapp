# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-31 11:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_module', '0005_exam_course_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exam',
            name='exam_date',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
