from rest_framework import serializers
from user_module.models import User, Exam, Course, Syllabus


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','user_name', 'first_name', 'last_name','course_type')

class ExamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Exam
        fields = ('id','exam_name', 'exam_date', 'course_type')

class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ('id', 'course_name')

class SyllabusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Syllabus
        fields = ('syllabus_file', 'course_type', 'course_name')
