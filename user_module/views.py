# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from user_module.models import User, Exam, Course, Syllabus
from user_module.serializers import UserSerializer, ExamSerializer, CourseSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse, StreamingHttpResponse
import os

# Create your views here.
def index(request):
    return render(request, 'user_module/index.html')


@api_view(['GET'])
def user_collection(request):
    if request.method == 'GET':
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        responseData = {}
        responseData['data'] = serializer.data
        return Response(responseData)

@api_view(['POST'])
def add_user(request):
    if request.method == 'POST':
        user = request.data.get('user_name')
        password = request.data.get('password')
        first_name = request.data.get('first_name')
        last_name = request.data.get('last_name')
        course_type = request.data.get('course_id')
        if isValidInformation(request.data):
            if not checkIfUserAlreadyExits(user):
                newUser = User(user_name=user, password=password, first_name=first_name, last_name=last_name, course_type_id=course_type)
                newUser.save()
                return Response({'status':'success', 'message':'user created'})
            else:
                return Response({'status':'error', 'message':'user already exists'})
        else:
            return Response({'status':'error', 'message':'params missing'})

@api_view(['POST'])
def add_exam(request):
    if request.method == 'POST':
        exam_name = request.data.get('exam_name')
        exam_date = request.data.get('exam_date')
        course_type = request.data.get('course_type')
        if exam_name is not None and exam_date is not None:
            examObj = Exam(exam_name=exam_name, exam_date=exam_date, course_type_id=course_type)
            examObj.save()
            return Response({'status':'success', 'message':'exam added'})
        else:
            return Response({'status':'error', 'message':'params missing'})

@api_view(['GET'])
def get_exam_list(request):
    if request.method == 'GET' and 'course_type' in request.GET:
        course_type = request.GET.get('course_type')
        if course_type is not None:
            if checkIfValidCourseID(course_type):
                courseObj = Course.objects.get(id=course_type)
                exam_list = courseObj.exams.all()
                serializer = ExamSerializer(exam_list, many=True)
                return Response({'status':'success', 'message':'List of exams found', 'data':serializer.data})
            return Response({'status':'error', 'message':'Invalid course id'})
        else:
            return Response({'status':'error', 'message':'params missing'})

@api_view(['GET'])
def get_course_list(request):
    if request.method == 'GET':
        course_list = Course.objects.all()
        serializer = CourseSerializer(course_list, many=True)
        return Response({'status':'success', 'message':'List of courses found', 'data':serializer.data})

@api_view(['POST'])
def add_course(request):
    if request.method == 'POST':
        course_name = request.data.get('course_name')
        if course_name is not None:
            if len(course_name) <= 10:
                if checkIfCourseExists(course_name):
                    return Response({'status':'success', 'message':'course already exists'})
                else:
                    courseObj = Course(course_name=course_name)
                    courseObj.save()
                    return Response({'status':'success', 'message':'course added'})
            else:
                return Response({'status':'error', 'message':'Course name length is too long(Max: 10 chars)'})
        else:
            return Response({'status':'error', 'message':'params missing'})

def checkIfCourseExists(course_name):
    try:
        course = Course.objects.get(course_name=course_name)
        return True
    except Course.DoesNotExist:
        return False

@api_view(['GET'])
def get_syllabus(request):
    if request.method == 'GET' and 'course_id' in request.GET:
        course_id = request.GET.get('course_id')
        if course_id is not None:
            if checkIfValidCourseID(course_id):
                if checkIfSyllabusAlreadyExists(course_id):
                    syllabusObj = Syllabus.objects.get(course_type=course_id)
                    syllabusFile = syllabusObj.syllabus_file
                    fileResponse = serve_syllabus_file(syllabusFile.name, syllabusFile.path)
                    return fileResponse
                return Response({'status':'error', 'message':'No syllabus found'})
            return Response({'status':'error', 'message':'invalid course id'})
    return Response({'status':'error', 'message':'missing query params'})

def serve_syllabus_file(filename, filepath):
   response = StreamingHttpResponse((line for line in open(filepath, 'rb')))
   response['Content-Disposition'] = "attachment; filename={0}".format(filename)
   response['Content-Length'] = os.path.getsize(filepath)
   return response

def remove_previous_syllabus_file(filepath):
     os.remove(filepath)

@api_view(['POST'])
def upload_syllabus(request):
    if request.method == 'POST':
        course_type = request.data.get('course_id')
        syllabus_file = request.FILES.get('syllabus_file')
        if course_type is not None:
            if syllabus_file is None:
                return Response({'status':'error', 'message':'syllabus file missing'})
            if checkIfValidCourseID(course_type):
                if checkIfSyllabusAlreadyExists(course_type):
                    syllabusObj = Syllabus.objects.get(course_type=course_type)
                    remove_previous_syllabus_file(syllabusObj.syllabus_file.path)
                    syllabusObj.syllabus_file = syllabus_file
                    syllabusObj.save()
                else:
                    course_name = Course.objects.get(id=course_type).course_name
                    syllabusObj = Syllabus(syllabus_file=syllabus_file, course_type_id=course_type, course_name=course_name)
                    syllabusObj.save()
                return Response({'status':'success', 'message':'syllabus updated'})
            else:
                return Response({'status':'error', 'message':'Invalid course id'})
        else:
            return Response({'status':'error', 'message':'params missing'})

def checkIfSyllabusAlreadyExists(course_type):
    try:
        syllabus = Syllabus.objects.get(course_type=course_type)
        return True
    except Syllabus.DoesNotExist:
        return False

def checkIfValidCourseID(course_id):
    try:
        int(course_id)
        course = Course.objects.get(id=course_id)
        return True
    except ValueError:
        return False
    except Course.DoesNotExist:
        return False



@api_view(['POST'])
def login(request):
    if request.method == 'POST':
        user = request.data.get('user_name')
        password = request.data.get('password')
        print(user)
        print(password)
        if user is not None and password is not None:
            if user == 'admin':
                if password == 'Admin@1234':
                    return Response({'status':'success', 'message':'login successful', 'data':createAdminData()})
                else:
                    return Response({'status':'error', 'message':'Unable to sign in as admin'})
            elif checkIfUserAlreadyExits(user):
                print('hello')
                userObj = User.objects.get(user_name=user)
                if userObj.password == password:
                    serializer = UserSerializer(userObj)
                    return Response({'status':'success', 'message':'login successful', 'data':serializer.data})
                else:
                    return Response({'status':'error', 'message':'Unable to sign in'})
            else:
                return Response({'status':'error', 'message':'No such user'})
        else:
            return Response({'status':'error', 'message':'params missing'})


def isValidInformation(data):
    if data.get('user_name') is None:
        return False
    if data.get('password') is None:
        return False
    if data.get('first_name') is None:
        return False
    if data.get('last_name') is None:
        return False
    if data.get('course_id') is None:
        return False
    return True

def createAdminData():
    adminUser = {}
    adminUser['first_name'] = 'Admin'
    adminUser['user_name'] = 'Admin'
    adminUser['last_name'] = 'StudentApp'
    adminUser['is_admin'] = True
    return adminUser



def checkIfUserAlreadyExits(user):
    try:
        user = User.objects.get(user_name=user)
        return True
    except User.DoesNotExist:
        return False
