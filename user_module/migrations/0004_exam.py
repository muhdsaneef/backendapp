# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-31 11:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_module', '0003_auto_20180317_1002'),
    ]

    operations = [
        migrations.CreateModel(
            name='Exam',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('exam_name', models.CharField(max_length=50)),
                ('exam_date', models.DateField(auto_now=True)),
            ],
        ),
    ]
