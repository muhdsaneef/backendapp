# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-31 11:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_module', '0004_exam'),
    ]

    operations = [
        migrations.AddField(
            model_name='exam',
            name='course_type',
            field=models.CharField(default='BSC', max_length=50),
            preserve_default=False,
        ),
    ]
