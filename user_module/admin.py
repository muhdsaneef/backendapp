# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from user_module.models import User, Exam, Course, Syllabus
# Register your models here.

admin.site.register(User)
admin.site.register(Exam)
admin.site.register(Course)
admin.site.register(Syllabus)
