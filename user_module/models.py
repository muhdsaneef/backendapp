# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

def get_file_path(instance, filename):
    return 'syllabus/{0}/{1}'.format(instance.course_name, filename)

# Create your models here.
class Course(models.Model):
    course_name = models.CharField(max_length=10)

    def __str__(self):
        return self.course_name

class User(models.Model):
    user_name = models.CharField(max_length=50)
    password = models.CharField(max_length=20)
    first_name = models.CharField(max_length=10)
    last_name = models.CharField(max_length=50)
    course_type = models.ForeignKey(Course, on_delete=models.CASCADE)

    def __str__(self):
        self.user_name

class Exam(models.Model):
    exam_name = models.CharField(max_length=50)
    exam_date = models.DateTimeField('Date added')
    course_type = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='exams')

    def __str__(self):
        return self.exam_name

class Syllabus(models.Model):
    syllabus_file = models.FileField(upload_to=get_file_path)
    course_type = models.ForeignKey(Course, on_delete=models.CASCADE)
    course_name = models.CharField(max_length=10)

    def __str__(self):
        return self.course_name
