# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-31 13:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_module', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='blood_group',
            field=models.CharField(default='A', max_length=3),
            preserve_default=False,
        ),
    ]
