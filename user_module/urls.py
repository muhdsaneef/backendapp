from django.conf.urls import url

from . import views

app_name = 'user_module'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^api/v1/users/$',views.user_collection, name='user_collection'),
    url(r'^api/v1/add_user/$',views.add_user, name='new_user'),
    url(r'^api/v1/login/$',views.login, name='login'),
    url(r'^api/v1/add_exam/$',views.add_exam, name='add_exam'),
    url(r'^api/v1/exam_list/$',views.get_exam_list, name='exam_list'),
    url(r'^api/v1/course_list/$',views.get_course_list, name='course_list'),
    url(r'^api/v1/add_course/$',views.add_course, name='add_course'),
    url(r'^api/v1/upload_syllabus/$',views.upload_syllabus, name='add_syllabus'),
    url(r'^api/v1/get_syllabus/$',views.get_syllabus, name='get_syllabus'),
]
